package com.rsa.routing.dao;

import com.rsa.routing.db.model.Routing;
import com.rsa.routing.entity.GIM;

public interface RoutingDAO {
	
	public Routing getRoutingInfo(GIM gim);

}
