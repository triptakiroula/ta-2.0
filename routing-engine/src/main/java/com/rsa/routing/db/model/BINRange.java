package com.rsa.routing.db.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BINRange")
public class BINRange {

	private int id;
	private String name;
	private Long low;
	private Long high;

	private Set<Routing> routes;

	/**
	 * Default Constructor
	 */
	public BINRange() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "binID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "low")
	public Long getLow() {
		return low;
	}

	public void setLow(Long low) {
		this.low = low;
	}

	@Column(name = "high")
	public Long getHigh() {
		return high;
	}

	public void setHigh(Long high) {
		this.high = high;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "binRanges")
	public Set<Routing> getRoutes() {
		return routes;
	}

	public void setRoutes(Set<Routing> routes) {
		this.routes = routes;
	}

}
