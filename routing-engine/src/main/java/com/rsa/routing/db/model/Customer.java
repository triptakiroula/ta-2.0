package com.rsa.routing.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*Typically a merchant*/
@Entity
@Table(name = "CUSTOMER")
public class Customer {

	private String id;
	private String name;

	/**
	 * Default Constructor
	 */
	public Customer() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customerID", unique = true, nullable = false)
	public String getId() {
		return id;
	}

	@Column(name = "customerName")
	public String getName() {
		return name;
	}


}
