package com.rsa.routing.db.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ROUTING")
public class Routing {

	private int id;

	private int priority;

	/* 0 means inactive rule ; 1 indicates active rule */
	private int status;

	/* Indicates the nature of transation i.e. Credit, Debit etc. */
	private String transactionType;

	/* Processor to forward to */
	private Processor processor;

	/* Customer for which the rule is applicable */
	private Customer customer;
	
	private Set<BINRange> ranges;

	/**
	 * Default Constructor
	 */
	public Routing() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "routingID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "priority")
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "transType")
	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processorId", nullable = false)
	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customerId", nullable = false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "BINRule", joinColumns = { 
			@JoinColumn(name = "ruleID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "binID", 
					nullable = false, updatable = false) })
	public Set<BINRange> getRanges() {
		return ranges;
	}

	public void setRanges(Set<BINRange> ranges) {
		this.ranges = ranges;
	}
	
	

}
