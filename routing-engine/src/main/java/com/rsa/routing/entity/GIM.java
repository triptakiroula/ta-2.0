package com.rsa.routing.entity;

import com.rsa.routing.db.model.Customer;
import com.rsa.routing.db.model.Routing;


/*Placeholder Object*/
public class GIM {
	
	private Customer customer;
	
	private Routing routing;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}
	
	

}
