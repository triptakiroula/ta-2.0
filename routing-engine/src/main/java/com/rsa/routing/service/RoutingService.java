package com.rsa.routing.service;

import com.rsa.routing.entity.GIM;

public interface RoutingService {
	
	public void performTransationRouting(GIM gim);

}
