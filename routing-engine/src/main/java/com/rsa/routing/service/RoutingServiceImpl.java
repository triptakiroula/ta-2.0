package com.rsa.routing.service;

import org.springframework.beans.factory.annotation.Required;

import com.rsa.routing.dao.RoutingDAO;
import com.rsa.routing.entity.GIM;

public class RoutingServiceImpl implements RoutingService {
	
	private RoutingDAO routingDAO;

	@Override
	public void performTransationRouting(GIM gim) {
		gim.setRouting(routingDAO.getRoutingInfo(gim));
		
	}

	@Required
	public void setRoutingDAO(RoutingDAO routingDAO) {
		this.routingDAO = routingDAO;
	}
	
	

}
